﻿namespace RegexDemo
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.telephone = new System.Windows.Forms.Label();
            this.pasport = new System.Windows.Forms.Label();
            this.number = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.mail = new System.Windows.Forms.Label();
            this.numberphone = new System.Windows.Forms.TextBox();
            this.numberpasport = new System.Windows.Forms.TextBox();
            this.numberinterval = new System.Windows.Forms.TextBox();
            this.ukrname = new System.Windows.Forms.TextBox();
            this.times = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.phonebutton = new System.Windows.Forms.Button();
            this.pasportbutton = new System.Windows.Forms.Button();
            this.numberbutton = new System.Windows.Forms.Button();
            this.namebutton = new System.Windows.Forms.Button();
            this.timebutton = new System.Windows.Forms.Button();
            this.mailbutton = new System.Windows.Forms.Button();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelPassport = new System.Windows.Forms.Label();
            this.labelNum = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.Label();
            this.price = new System.Windows.Forms.Label();
            this.dates = new System.Windows.Forms.TextBox();
            this.prices = new System.Windows.Forms.TextBox();
            this.datebutton = new System.Windows.Forms.Button();
            this.pricebutton = new System.Windows.Forms.Button();
            this.labelDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelprice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(96, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(592, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Демонстрація роботи з регулярними виразами";
            // 
            // telephone
            // 
            this.telephone.AutoSize = true;
            this.telephone.BackColor = System.Drawing.Color.Transparent;
            this.telephone.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telephone.Location = new System.Drawing.Point(65, 89);
            this.telephone.Name = "telephone";
            this.telephone.Size = new System.Drawing.Size(154, 22);
            this.telephone.TabIndex = 1;
            this.telephone.Text = "Номер телефону";
            // 
            // pasport
            // 
            this.pasport.AutoSize = true;
            this.pasport.BackColor = System.Drawing.Color.Transparent;
            this.pasport.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pasport.Location = new System.Drawing.Point(65, 140);
            this.pasport.Name = "pasport";
            this.pasport.Size = new System.Drawing.Size(153, 22);
            this.pasport.TabIndex = 2;
            this.pasport.Text = "Номер паспорта";
            // 
            // number
            // 
            this.number.AutoSize = true;
            this.number.BackColor = System.Drawing.Color.Transparent;
            this.number.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.number.Location = new System.Drawing.Point(65, 199);
            this.number.Name = "number";
            this.number.Size = new System.Drawing.Size(293, 22);
            this.number.TabIndex = 3;
            this.number.Text = "Число з інтервалу [10311;89645]";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.BackColor = System.Drawing.Color.Transparent;
            this.name.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.name.Location = new System.Drawing.Point(65, 262);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(142, 22);
            this.name.TabIndex = 4;
            this.name.Text = "Українське ім\'я";
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.BackColor = System.Drawing.Color.Transparent;
            this.time.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.time.Location = new System.Drawing.Point(65, 324);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(113, 22);
            this.time.TabIndex = 5;
            this.time.Text = "Введіть час";
            // 
            // mail
            // 
            this.mail.AutoSize = true;
            this.mail.BackColor = System.Drawing.Color.Transparent;
            this.mail.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mail.Location = new System.Drawing.Point(65, 386);
            this.mail.Name = "mail";
            this.mail.Size = new System.Drawing.Size(65, 22);
            this.mail.TabIndex = 6;
            this.mail.Text = "E-mail";
            // 
            // numberphone
            // 
            this.numberphone.Location = new System.Drawing.Point(377, 90);
            this.numberphone.Name = "numberphone";
            this.numberphone.Size = new System.Drawing.Size(126, 22);
            this.numberphone.TabIndex = 7;
            // 
            // numberpasport
            // 
            this.numberpasport.Location = new System.Drawing.Point(377, 142);
            this.numberpasport.Name = "numberpasport";
            this.numberpasport.Size = new System.Drawing.Size(126, 22);
            this.numberpasport.TabIndex = 8;
            // 
            // numberinterval
            // 
            this.numberinterval.Location = new System.Drawing.Point(377, 200);
            this.numberinterval.Name = "numberinterval";
            this.numberinterval.Size = new System.Drawing.Size(126, 22);
            this.numberinterval.TabIndex = 9;
            // 
            // ukrname
            // 
            this.ukrname.Location = new System.Drawing.Point(377, 264);
            this.ukrname.Name = "ukrname";
            this.ukrname.Size = new System.Drawing.Size(126, 22);
            this.ukrname.TabIndex = 10;
            // 
            // times
            // 
            this.times.Location = new System.Drawing.Point(377, 325);
            this.times.Name = "times";
            this.times.Size = new System.Drawing.Size(126, 22);
            this.times.TabIndex = 11;
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(377, 386);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(126, 22);
            this.email.TabIndex = 12;
            // 
            // phonebutton
            // 
            this.phonebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.phonebutton.Location = new System.Drawing.Point(561, 81);
            this.phonebutton.Name = "phonebutton";
            this.phonebutton.Size = new System.Drawing.Size(112, 33);
            this.phonebutton.TabIndex = 13;
            this.phonebutton.Text = "Перевірити";
            this.phonebutton.UseVisualStyleBackColor = true;
            this.phonebutton.Click += new System.EventHandler(this.phonebutton_Click);
            // 
            // pasportbutton
            // 
            this.pasportbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pasportbutton.Location = new System.Drawing.Point(561, 138);
            this.pasportbutton.Name = "pasportbutton";
            this.pasportbutton.Size = new System.Drawing.Size(112, 30);
            this.pasportbutton.TabIndex = 14;
            this.pasportbutton.Text = "Перевірити";
            this.pasportbutton.UseVisualStyleBackColor = true;
            this.pasportbutton.Click += new System.EventHandler(this.pasportbutton_Click);
            // 
            // numberbutton
            // 
            this.numberbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.numberbutton.Location = new System.Drawing.Point(561, 196);
            this.numberbutton.Name = "numberbutton";
            this.numberbutton.Size = new System.Drawing.Size(112, 31);
            this.numberbutton.TabIndex = 15;
            this.numberbutton.Text = "Перевірити";
            this.numberbutton.UseVisualStyleBackColor = true;
            this.numberbutton.Click += new System.EventHandler(this.numberbutton_Click);
            // 
            // namebutton
            // 
            this.namebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.namebutton.Location = new System.Drawing.Point(561, 259);
            this.namebutton.Name = "namebutton";
            this.namebutton.Size = new System.Drawing.Size(112, 32);
            this.namebutton.TabIndex = 16;
            this.namebutton.Text = "Перевірити";
            this.namebutton.UseVisualStyleBackColor = true;
            this.namebutton.Click += new System.EventHandler(this.namebutton_Click);
            // 
            // timebutton
            // 
            this.timebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.timebutton.Location = new System.Drawing.Point(561, 320);
            this.timebutton.Name = "timebutton";
            this.timebutton.Size = new System.Drawing.Size(112, 32);
            this.timebutton.TabIndex = 17;
            this.timebutton.Text = "Перевірити";
            this.timebutton.UseVisualStyleBackColor = true;
            this.timebutton.Click += new System.EventHandler(this.timebutton_Click);
            // 
            // mailbutton
            // 
            this.mailbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mailbutton.Location = new System.Drawing.Point(561, 381);
            this.mailbutton.Name = "mailbutton";
            this.mailbutton.Size = new System.Drawing.Size(112, 32);
            this.mailbutton.TabIndex = 18;
            this.mailbutton.Text = "Перевірити";
            this.mailbutton.UseVisualStyleBackColor = true;
            this.mailbutton.Click += new System.EventHandler(this.mailbutton_Click);
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.BackColor = System.Drawing.Color.Transparent;
            this.labelPhone.Location = new System.Drawing.Point(707, 89);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(0, 16);
            this.labelPhone.TabIndex = 19;
            // 
            // labelPassport
            // 
            this.labelPassport.AutoSize = true;
            this.labelPassport.BackColor = System.Drawing.Color.Transparent;
            this.labelPassport.Location = new System.Drawing.Point(707, 148);
            this.labelPassport.Name = "labelPassport";
            this.labelPassport.Size = new System.Drawing.Size(0, 16);
            this.labelPassport.TabIndex = 20;
            // 
            // labelNum
            // 
            this.labelNum.AutoSize = true;
            this.labelNum.BackColor = System.Drawing.Color.Transparent;
            this.labelNum.Location = new System.Drawing.Point(707, 203);
            this.labelNum.Name = "labelNum";
            this.labelNum.Size = new System.Drawing.Size(0, 16);
            this.labelNum.TabIndex = 21;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.BackColor = System.Drawing.Color.Transparent;
            this.labelName.Location = new System.Drawing.Point(707, 266);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(0, 16);
            this.labelName.TabIndex = 22;
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.BackColor = System.Drawing.Color.Transparent;
            this.labelTime.Location = new System.Drawing.Point(707, 328);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(0, 16);
            this.labelTime.TabIndex = 23;
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.BackColor = System.Drawing.Color.Transparent;
            this.labelEmail.Location = new System.Drawing.Point(707, 386);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(0, 16);
            this.labelEmail.TabIndex = 24;
            // 
            // date
            // 
            this.date.AutoSize = true;
            this.date.BackColor = System.Drawing.Color.Transparent;
            this.date.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.date.Location = new System.Drawing.Point(65, 444);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(298, 22);
            this.date.TabIndex = 25;
            this.date.Text = "Від 01.01.1900 р. до 31.12. 2017 р.";
            // 
            // price
            // 
            this.price.AutoSize = true;
            this.price.BackColor = System.Drawing.Color.Transparent;
            this.price.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.price.Location = new System.Drawing.Point(69, 502);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(50, 22);
            this.price.TabIndex = 26;
            this.price.Text = "Ціна";
            // 
            // dates
            // 
            this.dates.Location = new System.Drawing.Point(377, 443);
            this.dates.Name = "dates";
            this.dates.Size = new System.Drawing.Size(126, 22);
            this.dates.TabIndex = 27;
            // 
            // prices
            // 
            this.prices.Location = new System.Drawing.Point(377, 502);
            this.prices.Name = "prices";
            this.prices.Size = new System.Drawing.Size(126, 22);
            this.prices.TabIndex = 28;
            // 
            // datebutton
            // 
            this.datebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.datebutton.Location = new System.Drawing.Point(561, 438);
            this.datebutton.Name = "datebutton";
            this.datebutton.Size = new System.Drawing.Size(112, 32);
            this.datebutton.TabIndex = 29;
            this.datebutton.Text = "Перевірити";
            this.datebutton.UseVisualStyleBackColor = true;
            this.datebutton.Click += new System.EventHandler(this.datebutton_Click);
            // 
            // pricebutton
            // 
            this.pricebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pricebutton.Location = new System.Drawing.Point(561, 498);
            this.pricebutton.Name = "pricebutton";
            this.pricebutton.Size = new System.Drawing.Size(112, 32);
            this.pricebutton.TabIndex = 30;
            this.pricebutton.Text = "Перевірити";
            this.pricebutton.UseVisualStyleBackColor = true;
            this.pricebutton.Click += new System.EventHandler(this.pricebutton_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.Color.Transparent;
            this.labelDate.Location = new System.Drawing.Point(710, 443);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(0, 16);
            this.labelDate.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(437, 359);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 16);
            this.label2.TabIndex = 32;
            // 
            // labelprice
            // 
            this.labelprice.AutoSize = true;
            this.labelprice.BackColor = System.Drawing.Color.Transparent;
            this.labelprice.Location = new System.Drawing.Point(710, 502);
            this.labelprice.Name = "labelprice";
            this.labelprice.Size = new System.Drawing.Size(0, 16);
            this.labelprice.TabIndex = 33;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(874, 734);
            this.Controls.Add(this.labelprice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.pricebutton);
            this.Controls.Add(this.datebutton);
            this.Controls.Add(this.prices);
            this.Controls.Add(this.dates);
            this.Controls.Add(this.price);
            this.Controls.Add(this.date);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelNum);
            this.Controls.Add(this.labelPassport);
            this.Controls.Add(this.labelPhone);
            this.Controls.Add(this.mailbutton);
            this.Controls.Add(this.timebutton);
            this.Controls.Add(this.namebutton);
            this.Controls.Add(this.numberbutton);
            this.Controls.Add(this.pasportbutton);
            this.Controls.Add(this.phonebutton);
            this.Controls.Add(this.email);
            this.Controls.Add(this.times);
            this.Controls.Add(this.ukrname);
            this.Controls.Add(this.numberinterval);
            this.Controls.Add(this.numberpasport);
            this.Controls.Add(this.numberphone);
            this.Controls.Add(this.mail);
            this.Controls.Add(this.time);
            this.Controls.Add(this.name);
            this.Controls.Add(this.number);
            this.Controls.Add(this.pasport);
            this.Controls.Add(this.telephone);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(892, 781);
            this.Name = "Form1";
            this.Text = "Демонстрація роботи з регулярними виразами";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label telephone;
        private System.Windows.Forms.Label pasport;
        private System.Windows.Forms.Label number;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Label mail;
        private System.Windows.Forms.TextBox numberphone;
        private System.Windows.Forms.TextBox numberpasport;
        private System.Windows.Forms.TextBox numberinterval;
        private System.Windows.Forms.TextBox ukrname;
        private System.Windows.Forms.TextBox times;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Button phonebutton;
        private System.Windows.Forms.Button pasportbutton;
        private System.Windows.Forms.Button numberbutton;
        private System.Windows.Forms.Button namebutton;
        private System.Windows.Forms.Button timebutton;
        private System.Windows.Forms.Button mailbutton;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelPassport;
        private System.Windows.Forms.Label labelNum;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label date;
        private System.Windows.Forms.Label price;
        private System.Windows.Forms.TextBox dates;
        private System.Windows.Forms.TextBox prices;
        private System.Windows.Forms.Button datebutton;
        private System.Windows.Forms.Button pricebutton;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelprice;
    }
}

