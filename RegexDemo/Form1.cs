﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RegexDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string ok = "OK";
        string error = "Помилка";
        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void phonebutton_Click(object sender, EventArgs e)
        {
            Regex regPhone = new Regex(@"\+380\d\d\d\d\d\d\d\d\d");
            Match phoneMatch = regPhone.Match(numberphone.Text);
            if (phoneMatch.Success)
            {
                labelPhone.ForeColor = Color.Green;
                labelPhone.Text = ok;
            }
            else
            {
                labelPhone.ForeColor = Color.Red;
                labelPhone.Text = error;
            }
        }

        private void pasportbutton_Click(object sender, EventArgs e)
        {
            Regex regPassport = new Regex(@"(?:А[АВТЕКМОНСЮ]|В[АВЕНСТО]|Е[СКМНАВОР]|С[ЕСАВЮРКМТИНО]|К[АВСЕКМНСР]|М[ЕЮСКАНО]|Н[АЕСК]|ТТ) \d{6}"); ;
            Match passMatch = regPassport.Match(numberpasport.Text);
            if (passMatch.Success)
            {
                labelPassport.ForeColor = Color.Green;
                labelPassport.Text = ok;
            }
            else
            {
                labelPassport.ForeColor = Color.Red;
                labelPassport.Text = error;
            }
        }

        private void numberbutton_Click(object sender, EventArgs e)
        {
            Regex regNum = new Regex(@"^((1031[1-9])|(103[2-9]\d)|(10[4-9]\d{2})|(1[1-9]\d{3})|([2-7]\d{4})|(8964[0-5])|(896[0-3]\d)|(89[0-5]\d{2})|(8[0-8]\d{3}))$");
            Match numMatch = regNum.Match(number.Text);
            if (numMatch.Success)
            {
                labelNum.ForeColor = Color.Green;
                labelNum.Text = ok;
            }
            else
            {
                labelNum.ForeColor = Color.Red;
                labelNum.Text = error;
            }
        }

        private void namebutton_Click(object sender, EventArgs e)
        {
            Regex regName = new Regex(@"([А-ЯІҐЄ]{1}[а-яіґє]{1,30})");
            Match nameMatch = regName.Match(ukrname.Text);
            if (nameMatch.Success)
            {
                labelName.ForeColor = Color.Green;
                labelName.Text = ok;
            }
            else
            {
                labelName.ForeColor = Color.Red;
                labelName.Text = error;
            }
        }

        private void timebutton_Click(object sender, EventArgs e)
        {
            Regex regTime = new Regex(@"(?:1[0-9]|2[0-3]|0[0-9]):[0-5][0-9]");
            Match timeMatch = regTime.Match(times.Text);

            if (timeMatch.Success)
            {
                labelTime.ForeColor = Color.Green;
                labelTime.Text = ok;
            }

            else
            {
                labelTime.ForeColor = Color.Red;
                labelTime.Text = error;
            }
        }

        private void mailbutton_Click(object sender, EventArgs e)
        {
            Regex regEmail = new Regex("(?:[A-Za-z0-9_&]+)@(?:[a-z]+.(?:.com|.net|.ua|.com.ua))"); ;
            Match emailMatch = regEmail.Match(email.Text);

            if (emailMatch.Success)
            {
                labelEmail.ForeColor = Color.Green;
                labelEmail.Text = ok;
            }
            else
            {
                labelEmail.ForeColor = Color.Red;
                labelEmail.Text = error;
            }
        }

        private void datebutton_Click(object sender, EventArgs e)
        {
            Regex regDate = new Regex(@"(19\d\d|20[0-1][1-7])-((02-[0-2]\d|0[^2]-([0-2]\d|3[0-1]))|1[0-2]-([0-2]\d|3[0-1]))[ ][0-1]\d:([0-5]\d)|([6][0])|[2][0-4]:[0][0]");
            Match dateMatch = regDate.Match(dates.Text);

            if(dateMatch.Success)
            {
                labelDate.ForeColor = Color.Green;
                labelDate.Text = ok;
            }
            else
            {
                labelDate.ForeColor = Color.Red;
                labelDate.Text = error;
            }
        }

        private void pricebutton_Click(object sender, EventArgs e)
        {
            Regex regPrice = new Regex(@"((\d{1,}[ ]{0,1}[$])|(\d{1,}[.]{0,1}\d{1,}[ ]{0,1}грн))");
            Match priceMatch = regPrice.Match(prices.Text);

            if(priceMatch.Success)
            {
                labelprice.ForeColor = Color.Green;
                labelprice.Text = ok;
            }
            else
            {
                labelprice.ForeColor = Color.Red;
                labelprice.Text = error;
            }
        }
    }
}
