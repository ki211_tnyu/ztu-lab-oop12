﻿namespace Weather
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.update = new System.Windows.Forms.Button();
            this.Temperature = new System.Windows.Forms.Label();
            this.Wind = new System.Windows.Forms.Label();
            this.WindDirection = new System.Windows.Forms.Label();
            this.Humidity = new System.Windows.Forms.Label();
            this.Pressure = new System.Windows.Forms.Label();
            this.pictureBoxSky = new System.Windows.Forms.PictureBox();
            this.Cloud = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPressure = new System.Windows.Forms.Label();
            this.labelHumidity = new System.Windows.Forms.Label();
            this.labelWindDirection = new System.Windows.Forms.Label();
            this.labelWind = new System.Windows.Forms.Label();
            this.labelTemperature = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSky)).BeginInit();
            this.SuspendLayout();
            // 
            // update
            // 
            this.update.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.update.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.update.Font = new System.Drawing.Font("Times New Roman", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.update.Location = new System.Drawing.Point(167, 339);
            this.update.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(145, 32);
            this.update.TabIndex = 0;
            this.update.Text = "Оновити";
            this.update.UseVisualStyleBackColor = false;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // Temperature
            // 
            this.Temperature.AutoSize = true;
            this.Temperature.BackColor = System.Drawing.Color.Transparent;
            this.Temperature.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Temperature.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Temperature.Location = new System.Drawing.Point(273, 132);
            this.Temperature.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Temperature.Name = "Temperature";
            this.Temperature.Size = new System.Drawing.Size(0, 19);
            this.Temperature.TabIndex = 6;
            // 
            // Wind
            // 
            this.Wind.AutoSize = true;
            this.Wind.BackColor = System.Drawing.Color.Transparent;
            this.Wind.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Wind.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Wind.Location = new System.Drawing.Point(273, 169);
            this.Wind.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Wind.Name = "Wind";
            this.Wind.Size = new System.Drawing.Size(0, 19);
            this.Wind.TabIndex = 7;
            // 
            // WindDirection
            // 
            this.WindDirection.AutoSize = true;
            this.WindDirection.BackColor = System.Drawing.Color.Transparent;
            this.WindDirection.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WindDirection.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.WindDirection.Location = new System.Drawing.Point(273, 207);
            this.WindDirection.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.WindDirection.Name = "WindDirection";
            this.WindDirection.Size = new System.Drawing.Size(0, 19);
            this.WindDirection.TabIndex = 8;
            // 
            // Humidity
            // 
            this.Humidity.AutoSize = true;
            this.Humidity.BackColor = System.Drawing.Color.Transparent;
            this.Humidity.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Humidity.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Humidity.Location = new System.Drawing.Point(273, 246);
            this.Humidity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Humidity.Name = "Humidity";
            this.Humidity.Size = new System.Drawing.Size(0, 19);
            this.Humidity.TabIndex = 9;
            // 
            // Pressure
            // 
            this.Pressure.AutoSize = true;
            this.Pressure.BackColor = System.Drawing.Color.Transparent;
            this.Pressure.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Pressure.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Pressure.Location = new System.Drawing.Point(273, 284);
            this.Pressure.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Pressure.Name = "Pressure";
            this.Pressure.Size = new System.Drawing.Size(0, 19);
            this.Pressure.TabIndex = 10;
            // 
            // pictureBoxSky
            // 
            this.pictureBoxSky.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxSky.ImageLocation = "http://s1.gismeteo.ua/static/images/icons/new/n.moon.c4.png";
            this.pictureBoxSky.Location = new System.Drawing.Point(134, 63);
            this.pictureBoxSky.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBoxSky.Name = "pictureBoxSky";
            this.pictureBoxSky.Size = new System.Drawing.Size(75, 41);
            this.pictureBoxSky.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSky.TabIndex = 12;
            this.pictureBoxSky.TabStop = false;
            // 
            // Cloud
            // 
            this.Cloud.AutoSize = true;
            this.Cloud.BackColor = System.Drawing.Color.Transparent;
            this.Cloud.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Cloud.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Cloud.Location = new System.Drawing.Point(273, 79);
            this.Cloud.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Cloud.Name = "Cloud";
            this.Cloud.Size = new System.Drawing.Size(0, 19);
            this.Cloud.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(146, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 25);
            this.label1.TabIndex = 14;
            this.label1.Text = "Погода в Житомирі";
            // 
            // labelPressure
            // 
            this.labelPressure.AutoSize = true;
            this.labelPressure.BackColor = System.Drawing.Color.Transparent;
            this.labelPressure.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPressure.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelPressure.Location = new System.Drawing.Point(88, 281);
            this.labelPressure.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPressure.Name = "labelPressure";
            this.labelPressure.Size = new System.Drawing.Size(144, 19);
            this.labelPressure.TabIndex = 15;
            this.labelPressure.Text = "Атмосферний тиск";
            // 
            // labelHumidity
            // 
            this.labelHumidity.AutoSize = true;
            this.labelHumidity.BackColor = System.Drawing.Color.Transparent;
            this.labelHumidity.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelHumidity.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelHumidity.Location = new System.Drawing.Point(135, 243);
            this.labelHumidity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelHumidity.Name = "labelHumidity";
            this.labelHumidity.Size = new System.Drawing.Size(77, 19);
            this.labelHumidity.TabIndex = 16;
            this.labelHumidity.Text = "Вологість";
            // 
            // labelWindDirection
            // 
            this.labelWindDirection.AutoSize = true;
            this.labelWindDirection.BackColor = System.Drawing.Color.Transparent;
            this.labelWindDirection.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWindDirection.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelWindDirection.Location = new System.Drawing.Point(117, 204);
            this.labelWindDirection.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelWindDirection.Name = "labelWindDirection";
            this.labelWindDirection.Size = new System.Drawing.Size(103, 19);
            this.labelWindDirection.TabIndex = 17;
            this.labelWindDirection.Text = "Напрям вітру";
            // 
            // labelWind
            // 
            this.labelWind.AutoSize = true;
            this.labelWind.BackColor = System.Drawing.Color.Transparent;
            this.labelWind.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWind.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelWind.Location = new System.Drawing.Point(154, 166);
            this.labelWind.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelWind.Name = "labelWind";
            this.labelWind.Size = new System.Drawing.Size(49, 19);
            this.labelWind.TabIndex = 18;
            this.labelWind.Text = "Вітер";
            // 
            // labelTemperature
            // 
            this.labelTemperature.AutoSize = true;
            this.labelTemperature.BackColor = System.Drawing.Color.Transparent;
            this.labelTemperature.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTemperature.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTemperature.Location = new System.Drawing.Point(116, 128);
            this.labelTemperature.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTemperature.Name = "labelTemperature";
            this.labelTemperature.Size = new System.Drawing.Size(100, 19);
            this.labelTemperature.TabIndex = 19;
            this.labelTemperature.Text = "Температура";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.ButtonFace;
            this.linkLabel1.Location = new System.Drawing.Point(306, 404);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(147, 16);
            this.linkLabel1.TabIndex = 20;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Тишкевич Н.Ю КІ-21-1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(484, 436);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.labelTemperature);
            this.Controls.Add(this.labelWind);
            this.Controls.Add(this.labelWindDirection);
            this.Controls.Add(this.labelHumidity);
            this.Controls.Add(this.labelPressure);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cloud);
            this.Controls.Add(this.pictureBoxSky);
            this.Controls.Add(this.Pressure);
            this.Controls.Add(this.Humidity);
            this.Controls.Add(this.WindDirection);
            this.Controls.Add(this.Wind);
            this.Controls.Add(this.Temperature);
            this.Controls.Add(this.update);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximumSize = new System.Drawing.Size(502, 481);
            this.Name = "Form1";
            this.Text = "Погода в Житомирі";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSky)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Label Temperature;
        private System.Windows.Forms.Label Wind;
        private System.Windows.Forms.Label WindDirection;
        private System.Windows.Forms.Label Humidity;
        private System.Windows.Forms.Label Pressure;
        private System.Windows.Forms.PictureBox pictureBoxSky;
        private System.Windows.Forms.Label Cloud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPressure;
        private System.Windows.Forms.Label labelHumidity;
        private System.Windows.Forms.Label labelWindDirection;
        private System.Windows.Forms.Label labelWind;
        private System.Windows.Forms.Label labelTemperature;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

