﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassWeather;

namespace Weather
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Parcing();
        }

        private void update_Click(object sender, EventArgs e)
        {
            Parcing();
        }
        private void Parcing()
        {
            WeatherClass weather = new WeatherClass("https://ua.sinoptik.ua/%D0%BF%D0%BE%D0%B3%D0%BE%D0%B4%D0%B0-%D0%B6%D0%B8%D1%82%D0%BE%D0%BC%D0%B8%D1%80");
            pictureBoxSky.ImageLocation = "https:" + weather.Image;
            Cloud.Text = weather.Cloud;
            Temperature.Text = weather.Temperature + " C";
            Wind.Text = weather.Wind + " м/с";
            WindDirection.Text = weather.WindDirection;
            Pressure.Text = weather.Pressure + " мм.рт.ст";
            Humidity.Text = weather.Humidity + "%";
        }

    }
}
