﻿using System.Text;
using System.Text.RegularExpressions;
using System.Net;

namespace ClassWeather
{
    public class WeatherClass
    {
        public string Cloud { get; private set; }
        public string Image { get; private set; }
        public string Temperature { get; private set; }
        public string Wind { get; private set; }
        public string WindDirection { get; private set; }
        public string Humidity { get; private set; }
        public string Pressure { get; private set; }
        public WeatherClass(string url)
        {
            WebClient web = new WebClient();
            web.Encoding = Encoding.UTF8;
            string htmlcode;
            htmlcode = web.DownloadString(url);
            Regex regex;
            Match match;
            regex = new Regex("<td class=\"p[1-8]( bR)? cur\" > <div class=\"weatherIco .{4}\" title=\"([а-яїєі, -]+)\"><img class=\"weatherImg\" src=\"(\\/\\/sinst\\.fwdcdn\\.com\\/img\\/weatherImg\\/s\\/.{4}\\.gif)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
            match = regex.Match(htmlcode);
            Cloud = match.Groups[2].Value;
            Image = match.Groups[3].Value;
            regex = new Regex("<tr class=\"temperature\">.+<td class=\"p[1-8]( bR)? cur\" >([\\+-]\\d+)", RegexOptions.Multiline);
            match = regex.Match(htmlcode);
            Temperature = match.Groups[2].Value;
            regex = new Regex("<td class=\"p[1-8]( bR)? cur\" > <div data-tooltip=\"([а-яїєі, -]+), (\\d+\\.\\d)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
            match = regex.Match(htmlcode);
            Wind = match.Groups[3].Value;
            WindDirection = match.Groups[2].Value;
            regex = new Regex("<tr class=\"gray\">.+<tr>.+<td class=\"p[1-8]( bR)? cur\" >(\\d\\d).+<tr class=\"gray\">", RegexOptions.Multiline);
            match = regex.Match(htmlcode);
            Humidity = match.Groups[2].Value;
            regex = new Regex("<td class=\"p[1-8]( bR)? cur\" >(\\d{3})<\\/td>", RegexOptions.Multiline);
            match = regex.Match(htmlcode);
            Pressure = match.Groups[2].Value;
        }
    }
}


